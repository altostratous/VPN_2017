##############################################
# Sample client-side OpenVPN 2.0 config file #
# for connecting to multi-client server.     #
#                                            #
# This configuration can be used by multiple #
# clients, however each client should have   #
# its own cert and key files.                #
#                                            #
# On Windows, you might want to rename this  #
# file so it has a .ovpn extension           #
##############################################

# Specify that we are a client and that we
# will be pulling certain config file directives
# from the server.
client
key-direction 1
# Use the same setting as you are using on
# the server.
# On most systems, the VPN will not function
# unless you partially or fully disable
# the firewall for the TUN/TAP interface.
;dev tap
dev tun

# Windows needs the TAP-Win32 adapter name
# from the Network Connections panel
# if you have more than one.  On XP SP2,
# you may need to disable the firewall
# for the TAP adapter.
;dev-node MyTap

# Are we connecting to a TCP or
# UDP server?  Use the same setting as
# on the server.
proto tcp
;proto udp

# The hostname/IP and port of the server.
# You can have multiple remote entries
# to load balance between the servers.
;remote 130.185.122.179 443
remote 45.82.136.91 443
;remote my-server-2 1194

# Choose a random host from the remote
# list for load-balancing.  Otherwise
# try hosts in the order specified.
;remote-random

# Keep trying indefinitely to resolve the
# host name of the OpenVPN server.  Very useful
# on machines which are not permanently connected
# to the internet such as laptops.
resolv-retry infinite

# Most clients don't need to bind to
# a specific local port number.
nobind

# Downgrade privileges after initialization (non-Windows only)
;user nobody
;group nogroup

# Try to preserve some state across restarts.
persist-key
persist-tun

# If you are connecting through an
# HTTP proxy to reach the actual OpenVPN
# server, put the proxy server/IP and
# port number here.  See the man page
# if your proxy server requires
# authentication.
;http-proxy-retry # retry on connection failures
;http-proxy [proxy server] [proxy port #]

# Wireless networks often produce a lot
# of duplicate packets.  Set this flag
# to silence duplicate packet warnings.
;mute-replay-warnings

# SSL/TLS parms.
# See the server config file for more
# description.  It's best to use
# a separate .crt/.key file pair
# for each client.  A single ca
# file can be used for all clients.
ca ca.crt
cert client.crt
key client.key

# Verify server certificate by checking that the
# certicate has the correct key usage set.
# This is an important precaution to protect against
# a potential attack discussed here:
#  http://openvpn.net/howto.html#mitm
#
# To use this feature, you will need to generate
# your server certificates with the keyUsage set to
#   digitalSignature, keyEncipherment
# and the extendedKeyUsage to
#   serverAuth
# EasyRSA can do this for you.
remote-cert-tls server

# If a tls-auth key is used on the server
# then every client must also have the key.
tls-auth ta.key 1

# Select a cryptographic cipher.
# If the cipher option is used on the server
# then you must also specify it here.
# Note that v2.4 client/server will automatically
# negotiate AES-256-GCM in TLS mode.
# See also the ncp-cipher option in the manpage
cipher AES-256-CBC
auth SHA256

# Enable compression on the VPN link.
# Don't enable this unless it is also
# enabled in the server config file.
#comp-lzo

# Set log file verbosity.
verb 3

# Silence repeating messages
;mute 20

# non systemd-resolved users
;script-security 2
;up /etc/openvpn/update-resolv-conf
;down /etc/openvpn/update-resolv-conf

# systemd-resolved users
;script-security 2
;up /etc/openvpn/scripts/update-systemd-resolved
;down /etc/openvpn/scripts/update-systemd-resolved
;down-pre
;dhcp-option DOMAIN-ROUTE .

<ca>
-----BEGIN CERTIFICATE-----
MIIDSzCCAjOgAwIBAgIUBwpSFbWcgFeP7qUT2o7NNkJBBH8wDQYJKoZIhvcNAQEL
BQAwFjEUMBIGA1UEAwwLRWFzeS1SU0EgQ0EwHhcNMjAwNjIxMTIxMzAxWhcNMzAw
NjE5MTIxMzAxWjAWMRQwEgYDVQQDDAtFYXN5LVJTQSBDQTCCASIwDQYJKoZIhvcN
AQEBBQADggEPADCCAQoCggEBAL+CVQ90TGZjUO29jR8eijoHseqxXICu3vMG+OyV
S4TMbY+ugOLWQ8ZYB6alHrcsB/45kXpco+6wfNqvnKGYn3FzgtDkdzxlgNARmWmV
pqA3DCXPwjjGoeOJcf0sRwzQspBi9pA4lgx03+TkS8pxpVU27HNv97BqUxJttfN8
RhdXc7uY4qGHi6R3QjTNnW9TiZlSKXBDujTDHw6NCLVTvKFFmUdiDc1i09P7Rnjg
WKHAyrpoqyP1GdYoH1vp9JBxtzeW9NTIG+11Lrly5rVhdWQLwRIjvfVdfV7JVf6/
yHl765v+VLkUVRFlifzAfFh9TRTQy8FkrhGgZ15Hvxw3pEcCAwEAAaOBkDCBjTAd
BgNVHQ4EFgQUQgbc7GnZDlOCC3oXjwqA8CF7BTYwUQYDVR0jBEowSIAUQgbc7GnZ
DlOCC3oXjwqA8CF7BTahGqQYMBYxFDASBgNVBAMMC0Vhc3ktUlNBIENBghQHClIV
tZyAV4/upRPajs02QkEEfzAMBgNVHRMEBTADAQH/MAsGA1UdDwQEAwIBBjANBgkq
hkiG9w0BAQsFAAOCAQEAPWLpGowIOlKQ1YFM0aXRHWWy3OUCeFv2n6KXdGCkvVlr
KfGnn2GxJY4px1lD7pV5KByOKD3NoE9kIVFREbF2lrub1dRo6U9Ulm5gx41dLhOY
wTUsiOntgoJWFBtEC9hjXUUK4LoVBmO6QhuCKEduQtMOrOASgb/SnvJZu1PUv0l1
z2fgKwkZphwDjItyi7wcYDX52qpSCcRbeUZNMNArqDVw7VAi/P7dR0zK+26zJUfJ
B5bYFxdW03Nx5q07TUCbqSxZvig1fkWMURdxRgHD+nSXZ3coR/Dx8DTwGxgcjkQM
gX/hkX+Kc1Gom6jqy/UmxT7JON0JxY7X+tMWDNu6Lg==
-----END CERTIFICATE-----
</ca>
<cert>
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            9b:3e:8b:c3:6c:43:c6:9d:13:17:81:f6:16:19:87:1d
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: CN=Easy-RSA CA
        Validity
            Not Before: Jun 21 13:01:05 2020 GMT
            Not After : Sep 24 13:01:05 2022 GMT
        Subject: CN=client1
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (2048 bit)
                Modulus:
                    00:f7:e2:65:c2:56:98:45:61:b5:f6:16:d1:a0:b2:
                    ff:a5:df:f4:af:09:92:7b:f5:1d:32:26:1d:bd:dc:
                    e1:c6:7f:cf:5c:0b:7e:d3:7c:47:e3:ca:96:ad:48:
                    c6:17:79:bb:37:00:00:a5:73:d3:85:e9:2d:26:d2:
                    45:ae:98:d0:11:41:07:43:39:53:91:73:0f:8c:48:
                    c1:09:7a:93:86:f3:8e:a7:1e:cd:e6:e3:df:5f:3e:
                    b6:4f:b1:03:f3:c9:9e:d0:3b:b7:51:59:05:bb:88:
                    4e:c8:46:ca:0c:43:cb:9a:8a:b8:59:59:4f:72:be:
                    1a:e7:48:38:06:48:74:9e:ee:71:16:c3:06:60:d6:
                    09:35:cc:44:92:e1:c2:0a:70:f6:d8:33:e4:cb:dc:
                    89:3c:f7:1e:df:e6:f8:b3:68:6d:a1:fe:37:0d:16:
                    7e:8c:3c:cf:ec:93:0e:c0:4f:79:37:c8:48:b1:d4:
                    1a:ed:f9:ef:f4:92:73:10:2e:cc:01:36:fa:f1:b3:
                    4d:bc:f6:9e:2a:87:63:1b:be:45:09:8b:86:d1:2b:
                    f6:ee:66:8a:89:3a:cd:a9:e7:81:c4:06:87:f1:c3:
                    33:e2:e0:82:76:cc:9d:d0:75:b3:cd:30:ab:6d:62:
                    1a:16:7a:16:4f:ba:14:cc:c7:bd:5e:97:8c:85:bf:
                    b4:67
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Basic Constraints: 
                CA:FALSE
            X509v3 Subject Key Identifier: 
                90:3D:F9:33:D1:A9:A6:D3:B7:71:8C:13:19:4D:69:B2:D7:9C:C2:97
            X509v3 Authority Key Identifier: 
                keyid:42:06:DC:EC:69:D9:0E:53:82:0B:7A:17:8F:0A:80:F0:21:7B:05:36
                DirName:/CN=Easy-RSA CA
                serial:07:0A:52:15:B5:9C:80:57:8F:EE:A5:13:DA:8E:CD:36:42:41:04:7F

            X509v3 Extended Key Usage: 
                TLS Web Client Authentication
            X509v3 Key Usage: 
                Digital Signature
    Signature Algorithm: sha256WithRSAEncryption
         9c:dc:33:54:44:bf:b7:cf:95:da:5b:a7:2d:a5:ad:6c:57:23:
         c5:f6:f9:d8:fe:bf:29:b9:d8:0c:84:65:8f:9a:78:c2:ec:6d:
         dd:fa:d0:50:b1:e2:93:15:9a:cd:a4:80:11:13:37:73:bd:86:
         df:7b:a9:c4:4b:e5:b1:a3:15:18:74:80:a0:bd:d7:e9:ce:fa:
         70:20:eb:18:44:b5:64:50:9f:cb:ba:20:94:51:d5:8f:5b:54:
         8f:51:2f:80:35:da:16:73:45:56:8b:81:b3:c5:48:9b:3a:9a:
         f4:51:58:5f:f7:dc:03:14:6e:e6:7b:b8:7a:b8:c5:4d:5f:42:
         74:c5:d6:34:f1:b7:d1:8d:56:35:20:a6:82:b2:cf:2c:a5:59:
         37:22:e0:ce:6e:97:59:7b:cc:d4:be:37:4c:97:93:80:aa:bf:
         15:5c:e1:32:7a:cd:9b:12:f3:47:49:7e:a8:91:ff:83:b5:d0:
         2d:9c:04:1e:4a:ce:d3:53:ca:f6:fa:69:f0:8c:54:be:e7:7a:
         20:47:62:c2:85:b1:a2:a8:eb:2f:ae:3a:4c:39:3b:c9:d1:c3:
         dd:d5:29:96:61:ae:fa:a4:dd:ad:ea:82:62:fa:7d:dd:c3:c7:
         72:fa:4a:13:ec:40:4e:8c:c4:24:4c:5f:1b:39:68:4e:16:1c:
         5f:b9:01:c0
-----BEGIN CERTIFICATE-----
MIIDVjCCAj6gAwIBAgIRAJs+i8NsQ8adExeB9hYZhx0wDQYJKoZIhvcNAQELBQAw
FjEUMBIGA1UEAwwLRWFzeS1SU0EgQ0EwHhcNMjAwNjIxMTMwMTA1WhcNMjIwOTI0
MTMwMTA1WjASMRAwDgYDVQQDDAdjbGllbnQxMIIBIjANBgkqhkiG9w0BAQEFAAOC
AQ8AMIIBCgKCAQEA9+JlwlaYRWG19hbRoLL/pd/0rwmSe/UdMiYdvdzhxn/PXAt+
03xH48qWrUjGF3m7NwAApXPThektJtJFrpjQEUEHQzlTkXMPjEjBCXqThvOOpx7N
5uPfXz62T7ED88me0Du3UVkFu4hOyEbKDEPLmoq4WVlPcr4a50g4Bkh0nu5xFsMG
YNYJNcxEkuHCCnD22DPky9yJPPce3+b4s2htof43DRZ+jDzP7JMOwE95N8hIsdQa
7fnv9JJzEC7MATb68bNNvPaeKodjG75FCYuG0Sv27maKiTrNqeeBxAaH8cMz4uCC
dsyd0HWzzTCrbWIaFnoWT7oUzMe9XpeMhb+0ZwIDAQABo4GiMIGfMAkGA1UdEwQC
MAAwHQYDVR0OBBYEFJA9+TPRqabTt3GMExlNabLXnMKXMFEGA1UdIwRKMEiAFEIG
3Oxp2Q5Tggt6F48KgPAhewU2oRqkGDAWMRQwEgYDVQQDDAtFYXN5LVJTQSBDQYIU
BwpSFbWcgFeP7qUT2o7NNkJBBH8wEwYDVR0lBAwwCgYIKwYBBQUHAwIwCwYDVR0P
BAQDAgeAMA0GCSqGSIb3DQEBCwUAA4IBAQCc3DNURL+3z5XaW6ctpa1sVyPF9vnY
/r8pudgMhGWPmnjC7G3d+tBQseKTFZrNpIAREzdzvYbfe6nES+WxoxUYdICgvdfp
zvpwIOsYRLVkUJ/LuiCUUdWPW1SPUS+ANdoWc0VWi4GzxUibOpr0UVhf99wDFG7m
e7h6uMVNX0J0xdY08bfRjVY1IKaCss8spVk3IuDObpdZe8zUvjdMl5OAqr8VXOEy
es2bEvNHSX6okf+DtdAtnAQeSs7TU8r2+mnwjFS+53ogR2LChbGiqOsvrjpMOTvJ
0cPd1SmWYa76pN2t6oJi+n3dw8dy+koT7EBOjMQkTF8bOWhOFhxfuQHA
-----END CERTIFICATE-----
</cert>
<key>
-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQD34mXCVphFYbX2
FtGgsv+l3/SvCZJ79R0yJh293OHGf89cC37TfEfjypatSMYXebs3AAClc9OF6S0m
0kWumNARQQdDOVORcw+MSMEJepOG846nHs3m499fPrZPsQPzyZ7QO7dRWQW7iE7I
RsoMQ8uairhZWU9yvhrnSDgGSHSe7nEWwwZg1gk1zESS4cIKcPbYM+TL3Ik89x7f
5vizaG2h/jcNFn6MPM/skw7AT3k3yEix1Brt+e/0knMQLswBNvrxs0289p4qh2Mb
vkUJi4bRK/buZoqJOs2p54HEBofxwzPi4IJ2zJ3QdbPNMKttYhoWehZPuhTMx71e
l4yFv7RnAgMBAAECggEAHoQciuQmMsRopjcA74uZtU3wzWucQO5X/ch7eNgsY0dw
u6AeqOJv5zcVfZer60/tIEQ70CKGM8IU8OaJ9YiSdM1Qm5H/G7TSgS6jtQsGBmMN
eL1uE7KR2SPbRcxA9E4p4vC3DKis7/1D1ZwBW6ks84WIVFeOVE3a+gYF/6AD21y5
QCb5Ca6vtyGI2BtxAIAn7oVdJiwu6JDLn+BivhjOuAvRHdKDJ6UpBUXPSNxs6poH
OOo8jnDHLl02Ulf2lguHecJWW5gOMzjZajuPH3NTj/s+PCn/dmZRHdtHLQae9oT6
Xv6pyzz76CrypoWAQUK9p+a6bqb9kwBs7hNBa2gjwQKBgQD+yFjMEKiBQ91VSNl1
h4VGmMkTm5bsDlqNGwg23AF2sh7ne4skU95G0bW8vxxDK/getC3WU0ly1qjS+FAK
FZa/4jPOh9AnCMlMWo2uRz+qKbPnqMh293S7YGptRl4FRcJWPLrwiyAuCUshGn+Q
RBMHvO62rrze9NezXfJyQ1hjSQKBgQD5EZzU4eRhTaheCg4aICOloNoCs9jO6VJx
AIXUSwqqwdisjLutqnYfLOyo/OTYJ8Tv5PsBMAiFRFtzr1duFrZESO+QiNd2paTP
tTpyHDSqq1CET+QFmHGvJ/gIrXNUsLOK5bOR3Dpck4u0ihJ+U3qDWks0ThB+wo0r
biwJKt+qLwKBgFGR5wv+UVSBW71QQ37jUoIVrFrAt4L/3XHLs8eMjFNqF4bZAOUy
jLlzrpGtRekjjJ6X5Qz3QU6o9wgEQ+vWmZr5WsjGppe4smnlm/KuEO3zyAtk+DlO
ZGsRJUGzA6sAeIWPj/IvOLBEfW8twqV3hfg2Pt7x9roS/XQJofN/KC8xAoGBAMjx
psAxx1mhl7bXEe2OBHR6Q6lug/rbT0IQE+jk6iQ95JvBpyFDp60bR4xHDDJAU6jf
6opYwGcUcVvyGO2esUqCNZU8cektVrylEBREIVGYiYtWETdaeE3CR2Bo3+Vx8kib
bba+IO/TsiDkTGtS2kLjNE1ste4u9EoXxrtWtcDNAoGAGpgXOHjz26CqQLJOXyEi
1OJx8KMr2vprigto1rAtS8twL9ESbylRc+dksQhYwQUQ0Znj7A09mP2vhAj6ccbC
DPLxeKV858hWFb1hL5ihHgxrSUbF2QUsZs+TGIrA93YqLJ4L3qHtLGq+p7q5FdS6
0e78z7LsTio7k+0IGRMjzeQ=
-----END PRIVATE KEY-----
</key>
<tls-auth>
#
# 2048 bit OpenVPN static key
#
-----BEGIN OpenVPN Static key V1-----
a339770dc38ed09d7da5e677dbf8bbbc
0565996faae8284998d7b0e5b520077a
0af8f30fc73b55dd46c75474203d072f
1a5bfedc431c49f9d06f4e74f92e1102
7b5a856dc21743b93981ffdd3f160807
2cf6d261ffeeceab0c6deaee217e9e39
5436e3933ce6cbc2bc151691b8e83ebe
96826a99aea3ac7ca9d930e0a0e6189c
d0193081b057d25a2f3927848ac72435
51b3c84d9c4de54d17f629050b0f41f8
2eb38601bdbb5541b8597d64ea0b2b84
7b64df8d3ed2f2be5e1866aff36e344e
c645733b433984c3988c4bb0646ec217
0cedb6f1f8be2acc2919b15a6bf1e325
dc8b20ac2a73c8e8a80add40028a43b9
5f55e52f99ca748d6cf68ed48ed15213
-----END OpenVPN Static key V1-----
</tls-auth>
